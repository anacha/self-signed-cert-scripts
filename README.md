# [DRAFT]

# Self-Signed Certicate generator scritp and others.

```text
--> key_private.key (bin) --> key_private.pem (txt)
    |
    `--> x509.public ---> public.pem
    |
    `--> cert.pfx
    `--> cert.csr
    `--> cert.crt
```

## 1st

```shell
hostname="<the name, must be prefix-file name>"
export KEYPASSWORD="1234"
export PASSPHRASE="5678"

# openssl genrsa -aes256 -passout pass:1234 -out cha.pem 4096
# openssl genrsa -aes256 -passout env:KEYPASSWORD -out ca.key.pem 4096

openssl \
    genrsa \
    -aes128 \
    -passout env:KEYPASSWORD \
    -out "$hostname-server.key.pem" 1028

openssl \
    req \
    -x509 \
    -new \
    -days 365 \
    -key "$hostname-server.key.pem" \
    -passin env:KEYPASSWORD \
    -out "$hostname-server.crt"

openssl \
    pkcs12 \
    -export \
    -in "$hostname-server.crt" \
    -inkey "$hostname-server.key.pem" \
    -passin env:KEYPASSWORD \
    -out "$hostname-server.cert.pfx" \
    -passout env:PASSPHRASE
```

CN=er.co.th

**Remove Passphrase from Key**, used for server key in apache/nginx to avoid ask for the pass-phrase each time the web server is started

```shell
openssl \
    rsa \
    -in "$hostname-server.key.pem" \
    -passin env:KEYPASSWORD \
    -out "$hostname-server.key.pem.nodes"
```

## References

- [Getting Self Signed SSL Certificates (.pem and .pfx)](https://github.com/Azure/azure-xplat-cli/wiki/Getting-Self-Signed-SSL-Certificates-(.pem-and-.pfx))
- [Openssl - Manual:Req(1)](https://wiki.openssl.org/index.php/Manual:Req(1))
- [Setting up OpenSSL to Create Certificates](http://www.flatmtn.com/article/setting-openssl-create-certificates)
- [Generate CSR and private key with password with OpenSSL](https://security.stackexchange.com/questions/106525/generate-csr-and-private-key-with-password-with-openssl)
- [How to create a self-signed SSL Certificate](https://www.akadia.com/services/ssh_test_certificate.html)
- [How To Create a Self-Signed SSL Certificate for Nginx in Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-16-04)
- [HowTo: Create CSR using OpenSSL Without Prompt (Non-Interactive)](https://www.shellhacks.com/create-csr-openssl-without-prompt-non-interactive/)

## References II

- [Why Client-Side Certificate Authentication? Why nginx?](https://github.com/nategood/sleep-tight)
- [Client-side SSL](https://gist.github.com/mtigas/952344)
- [**Secure Private APIs with Client-Certificates and Nginx**](https://www.curry-software.com/en/blog/authenticate_and_encrypt_microservice_communication/)
- [OpenSSL Certificate Authority](https://jamielinux.com/docs/openssl-certificate-authority/)
- [**Securing HTTP Traffic to Upstream Servers**](https://www.nginx.com/resources/admin-guide/nginx-https-upstreams/)
- [**Securing TCP Traffic to Upstream Servers**](https://www.nginx.com/resources/admin-guide/nginx-tcp-ssl-upstreams/)
- [nginx client certificate authentication](https://www.djouxtech.net/posts/nginx-client-certificate-authentication/)

## LICESE

[MIT](LICENSE)

-- Root Pair - Create the CA Key and Certificate for signing Client Certs
_my-ca.key.pem
_my-ca.crt.pem

openssl genrsa -aes256 -out _my-ca.key.pem 4096
openssl req \
      -key _my-ca.key.pem \
      -new -x509 -days 7300 -sha256 -extensions v3_ca \
      -out _my-ca.crt.pem

-- Create the Server Key, CSR, and Certificate
server.key.pem
server.csr.pem
server.crt.pem
server.crt.pfx

openssl genrsa -aes256 -out server.key.pem 1024
openssl req \
      -key server.key.pem \
      -new -sha256 \
      -out server.csr.pem
openssl x509 \
      -req -days 365 -extensions v3_intermediate_ca \
      -set_serial 01 \
      -CA _my-ca.crt.pem \
      -CAkey _my-ca.key.pem \
      -in server.csr.pem \
      -out server.crt.pem
openssl pkcs12 \
      -export \
      -in server.crt.pem \
      -inkey server.key.pem \
      -out server.crt.pfx

-- Create the Client Key and CSR
client.key.pem
client.csr.pem
client.crt.pem
client.crt.pfx

openssl genrsa -aes256 -out client.key.pem 1024
openssl req \
      -key client.key.pem \
      -new -sha256 \
      -out client.csr.pem
openssl x509 \
      -req -days 3650 -extensions v3_intermediate_ca \
      -set_serial 01 \
      -CA _my-ca.crt.pem \
      -CAkey _my-ca.key.pem \
      -in client.csr.pem \
      -out client.crt.pem
openssl pkcs12 \
      -export \
      -in client.crt.pem \
      -inkey client.key.pem \
      -out client.crt.pfx

-- Sign the client certificate with our CA cert.
-- Unlike signing our own server cert, this is what we want to do.
++ client.csr.pem, ca.crt.pem, ca.key.pem
client.crt

openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt
openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt

openssl genrsa -aes128 -out 1.ca.key.pem 1024
openssl req -new -key 1.ca.key.pem -out 1.ca.csr.pem

openssl genrsa -aes128 -out 1.client.key.pem 1024
openssl req -new -key 1.client.key.pem -out 1.client.csr.pem
openssl x509 -req -days 365 -in 1.client.csr.pem -CA 1._my-ca.crt.pem -CAkey 1._my-ca.key.pem -set_serial 01 -out 1.client.crt.pem

```text

/C=TH/ST=Bangkok/L=BKK/O=Education Research Co.,Ltd./OU=EduDev/CN=EduDev/emailAddress=ana.cpe9@gmail.com

-- << Create Root pair. >>
   +-- 1.ca.key.pem
   +-- 1.ca.crt.pem
   `-- << Create Cert Request >>
       +-- << Server: Create Cert Request >>
       +-- << Clint: Create Cert Request >>
```